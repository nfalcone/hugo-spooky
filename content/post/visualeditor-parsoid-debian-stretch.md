+++
date = "2019-02-18T07:16:56-04:00"
draft = false
title = "Install Visual Editor and Parsoid MediaWiki Extensions on Debian Stretch"

+++

Documentation for installing these two pieces of software exist but seem a bit lacking since I was not successful without some modifications I have included below.

First install the VisualEditor plugin like any other MediaWiki plugin described [here](https://www.mediawiki.org/wiki/Extension:VisualEditor#Download) under the **For the General User** section.

Next we need to install **parsoid** which is the server piece to the VisualEditor plugin.
These commands are assumed you are running as root, if not add `sudo`.
```
apt install dirmngr
apt-key advanced --keyserver keys.gnupg.net --recv-keys 90E9F83F22250DD7
echo "deb https://releases.wikimedia.org/debian jessie-mediawiki main" | tee /etc/apt/sources.list.d/parsoid.list
apt install apt-transport-https
apt update && sudo apt install parsoid
```

Next the parsoid config, open `/etc/mediawiki/parsoid/config.yaml` with your favorite $EDITOR and make sure it has:
```
logging:
    level: info

services:
  - module: ../src/lib/index.js
    entrypoint: apiServiceWorker
    conf:
        mwApis:
        - uri: 'https://wiki-dev.example.com/api.php'
          domain: 'wiki-dev.example.com'
```

For **parsoid** to work correctly we need a newer version of nodejs than that ships with Debian Stretch.
```
apt-get -t stretch-backports install nodejs
```

Lastly add the following to your `LocalSettings.php` in your MediaWiki install folder.
```
wfLoadExtension( 'VisualEditor' );

// Enable by default for everybody
$wgDefaultUserOptions['visualeditor-enable'] = 1;

// Optional: Set VisualEditor as the default for anonymous users
// otherwise they will have to switch to VE
// $wgDefaultUserOptions['visualeditor-editor'] = "visualeditor";

// Don't allow users to disable it
$wgHiddenPrefs[] = 'visualeditor-enable';

// OPTIONAL: Enable VisualEditor's experimental code features
//$wgDefaultUserOptions['visualeditor-enable-experimental'] = 1;

 $wgVirtualRestConfig['modules']['parsoid'] = array(
       // URL to the Parsoid instance
       // Use port 8142 if you use the Debian package
       'url' => 'http://localhost:8142',
       // Parsoid "domain", see below (optional)
       //'domain' => 'localhost',
       // Parsoid "prefix", see below (optional)
       //'prefix' => 'localhost'
);
```
