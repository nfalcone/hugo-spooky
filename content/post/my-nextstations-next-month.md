+++
date = "2016-02-16T07:16:56-04:00"
draft = false
title = "My NeXTstations Regular, Turbo, Turbo Color"

+++
In honor of **NeXT Month** for the month of February on [/r/retrobattlestations](https://www.reddit.com/r/retrobattlestations)

![](/images/next1.jpg)


Here's a few pictures of my NeXT gear, I have a NeXTstation, NeXTstation Turbo, and NeXTstation Turbo Color. In the pics I was only able to get my Turbo mono to work. The regular NeXTstation I have is really just for parts and I am still trying to figure out why the Turbo Color does not turn on now (anyone have any ideas? tried new battery and psu).

The NeXTstation Turbo that works is running NeXTStep 3.3 (though I think I need to install the Y2K patch), it has a 250Mb hard drive with 16Mb of RAM. I have a few extra SCSI disks lying around I hope to install OpenSTEP on this soon.

Also I have these fancy cables that let me use the NeXTstations with a "regular" lcd screen. The NeXT Turbo Color is not very hard to get working on a "new" monitor as it uses a 13w3 adapter but the Mono cable was very hard to track down, because it is such an odd proprietary connector. The Monitor is a Dell 2007FPb which works with most Unix workstations with weird graphics adapters.

See the full album [here](/images/nextstations/index.html).
