+++
date = "2019-02-18T07:16:56-04:00"
draft = false
title = "Install DotNet3.5 Windows Server 2012"

+++

We have WSUS so we have to do this... I know there is a way to publish DotNet 3.5 via WSUS but this seems quicker for the once off times 3.5 is needed.

First get your Server 2012 R2 ISO and copy this folder
`D:\sources\sxs`  (repace D:\ with whatever your optical drive is) to your desktop

Then open powershell or CMD as admin and run:

```
dism.exe /online /enable-feature /all /featurename:NetFX3 /source:"c:\users\admin\desktop\sxs"
```

Thats it! Reboot and you should be all set.
