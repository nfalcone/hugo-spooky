+++
date = "2019-01-05T07:16:56-04:00"
draft = false
title = "Setting up a Syncthing Relay on Linux"

+++

![](/images/Syncthing.png)
Syncthing is an awesome tool for keeping a folder in sync among multiple computers or servers.  Its a great open source alternative to Resilo Sync (previously BTsync).  In order for computers to find each other to begin syncing folders the software relies on relays.  A nice way to give back to an open source software project like this is running a relay.  These work great on a VPS or similar computer with lots of fast bandwidth.  

Since the program is precompiled these directions should work on any Linux and with some modification BSD, these instructions specifically were tested on Debian 9.

## Installing strelaysrv

First we need to get the latest version of the strelay program:

```
wget https://github.com/syncthing/relaysrv/releases/download/v0.14.48/strelaysrv-linux-amd64-v0.14.48.tar.gz
```

Then we will extract and move it to `/usr/local/bin` (note this assumes root priv)

```
tar zxvf strelaysrv-linux-amd64-v0.14.48.tar.gz
mv strelaysrv-linux-amd64-v0.14.48/strelaysrv /usr/local/bin 
chmod +x /usr/local/bin/
```

Now we will make a user for this program to run as

```
useradd relaysrv
mkdir /etc/relaysrv
chown relaysrv /etc/relaysrv
```

## Running as a service

Now that the program is installed we will set it up to run in the background, the first method is a simple script.  The second method is a service file for Systemd.

### create little script

create a file and place the following in it:
```
#!/bin/sh
cd /etc/relaysrv
su - relaysrv -c "/usr/bin/screen -d -m -fa -S relaysrv /usr/local/bin/strelaysrv -keys=/etc/relaysrv -provided-by='My little syncthing relay'"
```
```
chmod +x syncthing_start.sh
./syncthing_start.sh
```
### or systemd way

create a file here: `/etc/systemd/system/relaysrv.service` and place the following in it:

```
[Unit]
Description=Syncthing Relay Daemon
After=syslog.target network.target remote-fs.target nss-lookup.target

[Service]
Type=simple
ExecStart=/usr/local/bin/strelaysrv -keys=/etc/relaysrv -provided-by='my little syncthing server'
User=relaysrv
[Install]
WantedBy=multi-user.target
```

then run

```
systemctl daemon-reload
systemctl enable relaysrv
systemctl start relaysrv
```

Either way you do it you should then see your syncthing relay pop up on here after a short time: [https://relays.syncthing.net/](https://relays.syncthing.net/)

More information can be found [here](https://docs.syncthing.net/users/relaying.html) and [here](https://docs.syncthing.net/users/strelaysrv.html)
