+++
date = "2017-04-11T07:16:56-04:00"
draft = false
title = "Shortel Phones DHCP Server Option 156 on Sophos XG"

+++
          
Not a lot of documentation seeems to exist yet for the Sophos XG firewalls, and what does exist is somewhat sparse.

For some of our smaller sites that do not have a full fledged Windows server providing DHCP, we use whatever is built into the firewall.

This can be added to your Sophos XG firewall in just two commands.

First open console on your Sophos XG, whether that is via the web console
![](/images/sophos1.png)

Or via SSH and select 4)
![](/images/sophos2.png)

Then we need to create the actual option for the DHCP config

```
system dhcp dhcp-options add optioncode 156 optionname Shoretel optiontype string  
```

<p>Then apply the config to one of our DHCP servers on the Firewall</p>

```
system dhcp dhcp-options binding add dhcpname PhoneDHCP optionname Shoretel value ftpservers=192.168.1.1,configServers=192.168.1.1
```

