+++
date = 2016-02-21T07:16:56-04:00
draft = false
title = "Building an OpenBSD Desktop (or laptop)"

+++
![](/images/openbsdlogo.gif)

**Note:**
This guide takes some liberties assuming you were able to install OpenBSD on your own. 
 Also this guide was written and tested on the latest at the time OpenBSD 5.8.  
 Also basic networking whether that is wired or wireless is needed to be setup, so that is up to you.


First when installing OpenBSD make sure you say to:
```
Do you expect to run the X Window System? [yes] yes
Do you want the X Window System to be started by xdm(1)? [no] no
(this is because we will use GDM for gnome 3)
```

##Firewall Setup
Before we start installing anything lets setup a basic firewall.

`vi /etc/pf.conf`
comment out or delete everything already in there, then add:
```
set block-policy drop
set skip on lo0
match in all scrub (no-df random-id max-mss 1440)
antispoof quick for (egress)
block in quick on egress from { no-route urpf-failed } to any
block in all
pass out quick inet keep state
```

##Packages
Now we have to setup a package mirror to install software, pick a mirror from [http://www.openbsd.org/ftp.html](http://www.openbsd.org/ftp.html)  And add it to root's `.profile`

`vi /root/.profile`
```
export pkg_path=http://ftp.usa.openbsd.org/pub/OpenBSD/`uname -r`/packages/`uname -m`/
```
You can reboot or log off and log back in as root for the changes to take affect.

Let's also patch the system to the latest and greatest, you can do this manually via signify and the errata on openbsd.org or use the `openup` utility from mtier.  For the best security you should be building the patches from source but for a laptop and quick setup mtier works well.

```
ftp https://stable.mtier.org/openup
mv openup /usr/local/sbin/
chmod +x /usr/local/sbin/openup
openup
```


Installing needed packages
```
pkg_add -I gnome toadd firefox youtube-dl vim emacs irssi scrot mplayer ubuntu-fonts
```

for `/etc/login.conf` so that programs can utilize 1Gb of RAM like firefox or chromium
```
default:\
...
	datasize-cur=1024M:\
	datasize-max=1024M:\
```

Now we have the right packages installed let's setup our rc.conf to run the right programs on bootup.

`vim /etc/rc.conf.local`
```
xdm_flags=NO  
gnome_enable=YES  
gdm_enable=YES
multicast_host=YES  
ntpd_flags="-s"  
hotplugd_flags=""  
pkg_scripts="messagebus avahi_daemon toad gdm"  
```
**note messagebus is the replacement name for dbus_daemon as of 5.7**

If you have a e new laptop and have noticed the fans running really loud or the battery life not so good, add this to `/etc/rc.conf.loal`

```
apmd_flags="-C"
```

##Networking
Since this is a laptop or desktop setup it may be likely you need to change the settings regularly for different wifi networks you encounter, for this I really like this wireless script by overrider on github.
```
pkg_add -v git
git clone https://github.com/overrider/wireless
cd wireless
mv wireless /usr/local/bin/
mv wireless.cfg /etc/
```
then edit `/etc/wireless.cfg` for your network's settings
and then run `wireless $yournetworkname`

setting up doas so you dont have to use root or sudo
`vi /etc/doas.conf`
```
permit nopass keepenv { ENV PS1 SSH_AUTH_SOCK } :wheel
```
or if you want to be asked for a password:
```
permit keepenv { ENV PS1 SSH_AUTH_SOCK } :wheel
```
Then just make sure your user is a member of the wheel group.


###Further reading:  
[http://www.openbsdjumpstart.org](http://www.openbsdjumpstart.org)  
[http://www.bsdnow.tv/tutorials/the-desktop-obsd](http://www.bsdnow.tv/tutorials/the-desktop-obsd)  
[http://www.gabsoftware.com/tips/tutorial-install-gnome-desktop-and-gnome-display-manager-on-openbsd-4-8/](http://www.gabsoftware.com/tips/tutorial-install-gnome-desktop-and-gnome-display-manager-on-openbsd-4-8/)  
[http://www.lounge.se/wiki2/show/GnomeOnOpenBSD](http://www.lounge.se/wiki2/show/GnomeOnOpenBSD)  
[http://www.badbug.id.au/doas-or-how-i-use-sudo-on-openbsd-5-8/](http://www.badbug.id.au/doas-or-how-i-use-sudo-on-openbsd-5-8/)  
