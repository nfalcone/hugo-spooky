+++
date = "2016-03-11T07:16:56-04:00"
draft = false
title = "PHP 5.6 on Ubuntu 14.04"

+++

Ubuntu 14.04 is an LTS and great, stable release of Ubuntu Linux (the server edition).
Though it was released in 2014, there are some issues with the age of packages available to it.

One is PHP.  The latest PHP for 14.04 is ~5.5.9 and is not scheduled to get a 5.6 package.

To fix this we have to add some PPAs for the backported versions.

First for PHP:
```
sudo add-apt-repository ppa:ondrej/php5-5.6
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install php5
```

Also you may need from this ppa:
```
sudo apt-get install php5-gd php5-intl
```

And then if you are like most people who run some form of LAMP stack you will also need a different version of apache.
```
sudo add-apt-repository ppa:ondrej/apache2
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install apache2
```

There you go, you now have PHP 5.6 on your Ubuntu 14.04 LTS server.

