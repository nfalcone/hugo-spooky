+++
date = "2017-04-11T07:16:56-04:00"
draft = false
title = "Uninstall Exchange 2010 - Could not find any Domain Controller in domain"

+++

After wrapping up an Exchange 2016 migration it was time to decommission our old Exchange 2010 server.</p>

To do so, it is as easy as going to **Control Panel** > **Programs and Features** > and **Uninstall Microsoft Exchange 2010**

The uninstall wizard handles all the Active Directory object cleanup and removes the old server from the hybrid exchange group.

When running though the wizard gives the error:

```
Could not find any available Domain Controller in domain DC=d,DC=domain,DC=com.
```

This is actually a really deceptive error.  After checking DNS settings and network connectivity, I thought to disable UAC.

***After disabling UAC and rebooting, the wizard went right through with no error.***

Thanks Microsoft! Would help if the error actually complained about permissions or something to really point to UAC being the culprit.

